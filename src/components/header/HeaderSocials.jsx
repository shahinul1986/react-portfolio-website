import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {FiGitlab} from 'react-icons/fi'
import {BsFacebook} from 'react-icons/bs'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://linkedin.com" target="_blank"><BsLinkedin/></a>
        <a href="https://gitlab.com" target="_blank"><FiGitlab/></a>
        <a href="https://facebook.com" target="_blank"><BsFacebook/></a>
    </div>
  )
}

export default HeaderSocials